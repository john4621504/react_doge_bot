// telegram-web-app.d.ts
interface Window {
    Telegram: {
      WebApp: {
        initData: any; // 请根据实际情况定义准确的类型
        initDataUnsafe: any; // 请根据实际情况定义准确的类型
        sendData: (data: string) => void;
        // ... 其他方法和属性
      };
    };
  }
  