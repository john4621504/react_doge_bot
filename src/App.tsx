import React from 'react';
// import logo from './logo.svg';
import './App.css';
// 導入 Content 組件所需的元素
// import { MainButton, useShowPopup } from '@vkruglikov/react-telegram-web-app';
import ShowPopupDemo from './ShowPopupDemo';

// Content 組件定義
// const Content = () => {
//   const showPopup = useShowPopup();

//   const handleClick = () => 
//     showPopup({
//       message: 'Hello, I am popup',
//     });

//   return <MainButton text="SHOW POPUP" onClick={handleClick} />;
// };

// App 組件
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="container"> {/* Add a container */}
          {/* 加一個外框 */}
          {/* <Content /> */}
          <ShowPopupDemo />
        </div>
      </header>
    </div>
  );
}

export default App;