import React, { FC, useState} from 'react';
import { Form, Input, Typography } from 'antd';
import { MainButton, useShowPopup } from '@vkruglikov/react-telegram-web-app';

const ShowPopupDemo: FC = () => {
  
  const showPopup = useShowPopup();
  const [optionsState, setOptionsState] = useState([
    { name: "op_filter", value: '9FEB24', placeholder: "op_filter ex:9FEB24" },
    { name: "port_invest_amount", value: '10000', placeholder: "port_invest_amount (usdc) ex: 10000" },
    { name: "option_nums", value: '2',palceholder:"Split option_nums ex:2" },
    { name: "weight_type", value: '1',palceholder:"weight_type ex 1: average 2: weighted" },
    { name: "low_bound", value: '20',palceholder:"target profit lower bound ex: 20 (%)" },
    { name: "high_bound", value: '30',palceholder:"target profit higher bound ex 30 (%)" },
  ]);

  const onFinish = async (values: any) => {
    setOptionsState(prevOptions => {
      return prevOptions.map(option => {
        return {
          ...option,
          value: values[option.name],
        };
      });
    });

     // 准备发送的数据
    const dataToSend = optionsState.map(option => {
      return {
        ...option,
        value: values[option.name],
      };
    });


    try {
      if (window.Telegram) {

        await window.Telegram.WebApp.sendData(JSON.stringify(dataToSend));

      } else {
        console.error('Telegram object is not found on the window object');
      }
    } catch (e) {
      let errorMessage = 'An unknown error occurred';

      // 检查 e 是否是一个 Error 对象
      if (e instanceof Error) {
        errorMessage = e.message;
      }
    
      showPopup({
        title: 'Error',
        message: errorMessage,
        buttons: [{ type: 'ok' }],
      });
    }
  };

  const initialValues = optionsState.reduce((acc: { [key: string]: any }, option) => {
    acc[option.name] = option.value;
    return acc;
  }, {});

  const [form] = Form.useForm(); // 使用 Form.useForm 创建表单实例
  
  const handleSubmit = () => {
    // 使用表单实例的方法来提交表单
    form
      .validateFields()
      .then(values => {
        // 表单验证成功后的操作
        form.submit();
      })
      .catch(info => {
        // 处理表单验证失败的情况
        console.error('Validate Failed:', info);
      });
  };
 
  return (
    <>
      <Typography.Title level={3} style={{ color: 'white' }}>Options Criteria</Typography.Title>
      <Form
        form={form} // 将表单实例传递给 Form 组件
        labelCol={{ span: 6 }}
        name="ShowPopupDemo"
        layout="horizontal"
        initialValues={initialValues}
        onFinish={onFinish}
        autoComplete="off"
      >
        {optionsState.map((option) => (
          <Form.Item label={option.name} name={option.name} key={option.name}>
            <Input placeholder={option.placeholder} />
          </Form.Item>
        ))}
        <Form.Item>
          <MainButton text="Search Positions" onClick={handleSubmit} />
        </Form.Item>
      </Form>
    </>
  );
};

export default ShowPopupDemo;
